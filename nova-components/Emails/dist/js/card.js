/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(6);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

Nova.booting(function (Vue, router, store) {
  Vue.component('emails', __webpack_require__(2));
});

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(4)
/* template */
var __vue_template__ = __webpack_require__(5)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/Card.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b9bc2c0a", Component.options)
  } else {
    hotAPI.reload("data-v-b9bc2c0a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 3 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['card'],

    data: function data() {
        return {
            domainFilter: null,
            results: this.card.domains
        };
    },
    mounted: function mounted() {
        //
    },


    methods: {
        filterDomains: function filterDomains() {
            if (!this.domainFilter || this.domainFilter === '') {
                this.results = this.card.domains;
            } else {
                var filter = this.domainFilter.toLowerCase();
                var domains = [];

                for (var i = 0; i < this.card.domains.length; i++) {
                    if (this.card.domains[i].domain.toLowerCase().indexOf(filter) > -1) {
                        domains.push(this.card.domains[i]);
                    }
                }

                this.results = domains;
            }
        }
    }
});

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "card",
    { staticClass: "flex flex-col items-center justify-center h-auto" },
    [
      _c("div", { staticClass: "px-3 py-3" }, [
        _c("h1", { staticClass: "text-center text-3xl text-80 font-light" }, [
          _vm._v("Email Domains")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "px-3 py-3" }, [
        _c("div", { staticClass: "w-full flex" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.domainFilter,
                expression: "domainFilter"
              }
            ],
            staticClass:
              "w-2/3 form-control form-input form-input-bordered mr-1",
            attrs: { type: "text", "aria-label": "Filter email domains" },
            domProps: { value: _vm.domainFilter },
            on: {
              keyup: function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                ) {
                  return null
                }
                return _vm.filterDomains($event)
              },
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.domainFilter = $event.target.value
              }
            }
          }),
          _vm._v(" "),
          _c("div", { staticClass: "w-1/3" }, [
            _c(
              "a",
              {
                staticClass: "btn btn-default btn-primary",
                on: {
                  click: function($event) {
                    return _vm.filterDomains()
                  }
                }
              },
              [_vm._v("Filter")]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "px-3 py-3 w-full" }, [
        _c("div", { staticClass: "text-center" }, [
          _c("span", { staticClass: "font-thin text-sm" }, [
            _vm._v("Showing top 10 domains")
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "flex w-full flex-wrap", attrs: { role: "list" } },
          _vm._l(_vm.results, function(domain, i) {
            return i < 10
              ? _c(
                  "div",
                  {
                    staticClass: "flex py-2 w-1/2 border-b-2 border-primary-30%"
                  },
                  [
                    _c("div", { staticClass: "w-3/4 font-bold truncate" }, [
                      _vm._v(_vm._s(domain.domain))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "w-1/4" }, [
                      _vm._v(_vm._s(domain.count))
                    ])
                  ]
                )
              : _vm._e()
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "px-3 py-3 w-full" }, [
        _c(
          "div",
          {
            staticClass:
              "bg-primary-10% flex items-center justify-center rounded"
          },
          [
            _c("div", { staticClass: "px-3 py-3 text-center" }, [
              _c("div", [
                _c("span", { staticClass: "font-bold text-lg" }, [
                  _vm._v(_vm._s(_vm.card.total_emails))
                ])
              ]),
              _vm._v(" "),
              _c("div", [_c("span", [_vm._v("Total Emails")])])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "px-3 py-3 text-center" }, [
              _c("div", [
                _c("span", { staticClass: "font-bold text-lg" }, [
                  _vm._v(_vm._s(_vm.card.total_domains))
                ])
              ]),
              _vm._v(" "),
              _c("div", [_c("span", [_vm._v("Unique Domains")])])
            ])
          ]
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b9bc2c0a", module.exports)
  }
}

/***/ }),
/* 6 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);