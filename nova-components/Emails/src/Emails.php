<?php

namespace User\Emails;

use App\User;
use Laravel\Nova\Card;

class Emails extends Card
{
    /**
     * The width of the card (1/3, 1/2, or full).
     *
     * @var string
     */
    public $width = '1/3';

    /**
     * Get the component name for the element.
     *
     * @return string
     */
    public function component()
    {
        return 'emails';
    }

    /**
     * Indicates that the analytics should show current visitors.
     *
     * @return $this
     */
    public function domainBreakdown()
    {
        $domains = User::selectRaw("SUBSTRING_INDEX(email,'@',-1) AS domain, COUNT(*) as count")
            ->groupBy('domain')
            ->orderBy('count', 'desc')
            ->get();

        return $this->withMeta([
            'domains'       => $domains,
            'total_domains' => $domains->count(),
            'total_emails'  => User::all()->count()
        ]);
    }
}
